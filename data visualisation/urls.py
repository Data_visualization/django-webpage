from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    # Examples:
     url(r'^$', 'login.views.home', name='home'),
     url(r'^EmbededApi', 'login.views.EmbededApi', name='Analytics'),
     url(r'^accounts/', include('registration.backends.default.urls')),

     url(r'^accounts/profile/',
     	TemplateView.as_view(template_name='profile.html'),
     	name='profile'),

     url(r'^$',
        TemplateView.as_view(template_name='index.html'),
        name='index'),

  
      url(r'^D3graphs',
        TemplateView.as_view(template_name='page.html'),
        name='D3.js'),

      url(r'^areachart',
        TemplateView.as_view(template_name='AreaChart.html'),
        name='AreaChart'),


    url(r'^admin/', include(admin.site.urls)),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
