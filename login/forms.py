from django import forms

from .models import SignUp


class SignUpForm(forms.ModelForm):
	class Meta:
		model = SignUp
		fields =['full_name','email']
		

	def clean_email(self):
		email = self.cleaned_data.get('email')
		email_base, provider = email.split("@")
		domain, extension = provider.split('.')
		if not domain == 'ayruz':
			raise forms.ValidationError("Please enter a valid domain address")
		if not extension == "com":
			raise forms.ValidationError("Please use a valid  email address")
		return email

#	def clean_full_name(self):
#		full_name = self.cleaned_data.get('full_name')
#		first_name, last_name = full_name.split(" ")
#		if not len(first_name) >= 2:
#			raise forms.ValidationError("please enter a valid name")
#		return full_name