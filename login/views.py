from django.shortcuts import render

# Create your views here.
def home(request):
	title = 'welcome  user.....  please log in'
	if request.user.is_authenticated():
		title = "welcome %s" %(request.user)


	context = {
		"title":title,
	}
	return render(request, "home.html", context)



# calling template for Embeded Api
def EmbededApi(request):
	return render(request, "EmbededApi.html", {})