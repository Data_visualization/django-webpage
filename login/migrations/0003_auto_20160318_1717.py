# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('newsletter', '0002_auto_20160317_1721'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='SingUp',
            new_name='SignUp',
        ),
    ]
